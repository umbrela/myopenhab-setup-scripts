# Steps

* Setup
```
sudo apt update && sudo apt install git -y
```

* Git clone this repo
```
git clone https://bitbucket.org/umbrela/myopenhab-setup-scripts
```

* Run script
```
cd myopenhab-setup-scripts
chmod +x setup.sh
./setup.sh
```
